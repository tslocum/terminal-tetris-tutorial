package main

import (
	"fmt"
	"log"
)

func main() {
	minos, err := Generate(4)
	if err != nil {
		log.Fatalf("failed to generate minos: %s", err)
	}

	for i, mino := range minos {
		if i > 0 {
			fmt.Println()
			fmt.Println()
		}

		fmt.Print(mino.Render())
	}
}
