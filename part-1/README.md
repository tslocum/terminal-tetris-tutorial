# Terminal-based Tetris Part 1
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Code for [Terminal-based Tetris part 1](https://rocketnine.space/post/tetris-1/).

## Run

```
go run gitlab.com/tslocum/tutorial-terminal-tetris/part-1
```
