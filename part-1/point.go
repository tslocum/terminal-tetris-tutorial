package main

// Point is an X,Y coordinate.
type Point struct {
	X, Y int
}

func (p Point) Rotate90() Point  { return Point{p.Y, -p.X} }
func (p Point) Rotate180() Point { return Point{-p.X, -p.Y} }
func (p Point) Rotate270() Point { return Point{-p.Y, p.X} }
func (p Point) Reflect() Point   { return Point{-p.X, p.Y} }

//Neighborhood returns the Von Neumann neighborhood of a Point.
func (p Point) Neighborhood() []Point {
	return []Point{
		{p.X - 1, p.Y},
		{p.X, p.Y - 1},
		{p.X + 1, p.Y},
		{p.X, p.Y + 1}}
}
