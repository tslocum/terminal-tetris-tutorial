# Terminal-based Tetris
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Code repository for the tutorial series [Terminal-based Tetris](https://rocketnine.space/post/tetris-1/).
