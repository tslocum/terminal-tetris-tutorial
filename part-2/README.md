# Terminal-based Tetris Part 2
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Code for [Terminal-based Tetris part 2](https://rocketnine.space/post/tetris-2/).

## Run

```
go run gitlab.com/tslocum/tutorial-terminal-tetris/part-2
```
