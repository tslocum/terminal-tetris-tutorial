package main

import (
	"fmt"
)

func main() {
	matrix := NewMatrix(10, 20, 20)

	matrix.M[I(2, 0, matrix.W)] = BlockSolidBlue

	matrix.M[I(5, 2, matrix.W)] = BlockSolidCyan

	matrix.M[I(4, 5, matrix.W)] = BlockSolidRed

	matrix.M[I(1, 7, matrix.W)] = BlockSolidYellow

	matrix.M[I(3, 15, matrix.W)] = BlockSolidMagenta

	fmt.Print(matrix.Render())
}
