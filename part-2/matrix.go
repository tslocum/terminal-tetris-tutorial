package main

import (
	"log"
	"strings"
)

// Matrix is a grid of Blocks.
type Matrix struct {
	W int // Width
	H int // Height
	B int // Buffer height

	M []Block // Contents
}

// NewMatrix returns a new Matrix.
func NewMatrix(width int, height int, buffer int) *Matrix {
	m := Matrix{
		W: width,
		H: height,
		B: buffer,
		M: make([]Block, width*(height+buffer)),
	}

	return &m
}

// I returns the index within a Matrix corresponding to the specified coordinates.
func I(x int, y int, width int) int {
	if x < 0 || x >= width || y < 0 {
		log.Panicf("failed to retrieve index for %d,%d width %d: invalid coordinates", x, y, width)
	}

	return (y * width) + x
}

// Block returns the Block at the specified coordinates, or BlockNone.
func (m *Matrix) Block(x int, y int) Block {
	if y >= m.H+m.B {
		log.Panicf("failed to retrieve block at %d,%d: invalid y coordinate", x, y)
	}

	index := I(x, y, m.W)
	return m.M[index]
}

// Render returns a visual representation of a Matrix.
func (m *Matrix) Render() string {
	var b strings.Builder

	for y := m.H - 1; y >= 0; y-- {
		b.WriteRune('<')
		b.WriteRune('!')

		for x := 0; x < m.W; x++ {
			block := m.Block(x, y)
			if block != BlockNone {
				b.WriteRune('X')
			} else {
				b.WriteRune(' ')
			}
		}

		b.WriteRune('!')
		b.WriteRune('>')
		b.WriteRune('\n')
	}

	// Bottom border

	b.WriteRune('<')
	b.WriteRune('!')

	for x := 0; x < m.W; x++ {
		b.WriteRune('=')
	}

	b.WriteRune('!')
	b.WriteRune('>')
	b.WriteRune('\n')

	b.WriteString(`  \/\/\/\/\/`)
	b.WriteRune('\n')

	return b.String()
}
