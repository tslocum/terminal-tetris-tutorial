package main

// Block represents the content (or lack thereof) of a single location within a Matrix.
type Block int

const (
	BlockNone Block = iota
	BlockSolidBlue
	BlockSolidCyan
	BlockSolidRed
	BlockSolidYellow
	BlockSolidMagenta
	BlockSolidGreen
	BlockSolidOrange
)
